package awacademy.de.MoMaBlog.nutzer;

import awacademy.de.MoMaBlog.beitrag.Beitrag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

import java.util.List;
import java.util.Optional;


@Controller
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registration", new RegistrationDTO("", "", ""));
        return "register";
    }


    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("registration") RegistrationDTO registration, BindingResult bindingResult) {

        if(!registration.getPassword1().equals(registration.getPassword2())) {
            bindingResult.addError(new FieldError("registration", "password2", "Stimmt nicht überein"));
        }

        if(bindingResult.hasErrors()) {
            return "register";
        }

        User user = new User(registration.getUsername(), registration.getPassword1());
        userRepository.save(user);

        return "redirect:/";
    }

    @GetMapping("/admin")
    public String showAdmin(@ModelAttribute("sessionUser") User sessionUser, Model model){
        if (sessionUser != null){
            if (sessionUser.isAdmin()){
                Beitrag beitrag = new Beitrag();
                model.addAttribute(beitrag);

                List<User> userList = userRepository.findAll();
                model.addAttribute("users", userList);
                return "adminSeite";
            }
            return "redirect:/";
        }
        return "redirect:/";
    }

    @PostMapping("/admin")
    public String admin(@ModelAttribute("sessionUser") User sessionUser, @ModelAttribute("userId") long userId){
        if(sessionUser != null){
                if (sessionUser.isAdmin()){
                Optional<User> optionalUser = userRepository.findById(userId);
                if (optionalUser.isPresent()){
                    optionalUser.get().setAdmin(true);
                    userRepository.save(optionalUser.get());
                    return "redirect:/admin";
                }
            }
        }
        return "redirect:/";
    }
}