package awacademy.de.MoMaBlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoMaBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoMaBlogApplication.class, args);
	}

}
