package awacademy.de.MoMaBlog.kommentare;

import awacademy.de.MoMaBlog.beitrag.Beitrag;
import awacademy.de.MoMaBlog.nutzer.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;


    private String kommentartext;


    /**
     * @ManyToOne setzt Mapping bzgl. Beziehung fest --> ein Beitrag kann mehrere Kommentare haben
     */
    @ManyToOne
    private Beitrag beitrag;

    @ManyToOne
    private User user;



    public Kommentar() {
    }

    public Kommentar(String kommentartext){
        this.kommentartext = kommentartext;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getKommentartext() {
        return kommentartext;
    }

    public void setKommentartext(String kommentartext) {
        this.kommentartext = kommentartext;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }
}
