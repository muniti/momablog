package awacademy.de.MoMaBlog.kommentare;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KommentarRepository extends CrudRepository<Kommentar, Integer> {

        List<Kommentar> findAll();
        List<Kommentar> findAllByBeitragId(int id);

}
