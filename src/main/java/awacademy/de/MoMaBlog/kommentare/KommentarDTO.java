package awacademy.de.MoMaBlog.kommentare;

public class KommentarDTO {

    private String kommentartext;

    public KommentarDTO(String kommentartext) {
        this.kommentartext = kommentartext;
    }


    public String getKommentartext() {
        return kommentartext;
    }

    public void setKommentartext(String kommentartext) {
        this.kommentartext = kommentartext;
    }
}
