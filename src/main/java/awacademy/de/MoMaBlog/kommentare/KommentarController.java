package awacademy.de.MoMaBlog.kommentare;

import awacademy.de.MoMaBlog.beitrag.Beitrag;
import awacademy.de.MoMaBlog.beitrag.BeitragRepository;
import awacademy.de.MoMaBlog.nutzer.User;
import awacademy.de.MoMaBlog.nutzer.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import java.util.Optional;

@Controller
public class KommentarController {

    private KommentarRepository kommentarRepository;
    private BeitragRepository beitragRepository;
    private UserRepository userRepository;

    @Autowired
    public KommentarController(KommentarRepository kommentarRepository,BeitragRepository beitragRepository, UserRepository userRepository) {
        this.kommentarRepository = kommentarRepository;
        this.beitragRepository = beitragRepository;
        this.userRepository = userRepository;
    }


    /**
     *Folgendes GetMapping zeigt das Formular zum Kommentarhinzufuegen an
     * @return
     */
    @GetMapping("/{beitragId}/kommentar/{userId}")
    public String create(Model model, @PathVariable Integer beitragId, @PathVariable Long userId) {
        model.addAttribute("kommentarDTO", new KommentarDTO(""));
        model.addAttribute("beitragId", beitragId);
        model.addAttribute("userId", userId);
        return "KommentarHinzufuegen";
    }

    /**
     * Methode erstellt einen neuen Kommentar zu einem Beitrag
     * @param beitragId, Kommentar wird mit jeweiliger BeitragsId verknüpft
     * @return: ein redirect wird ausgeführt, um das Double Submission Problem zu vermeiden
     */
    @PostMapping("/{beitragId}/kommentar/{userId}")
    public String submit(@ModelAttribute KommentarDTO kommentarDTO, @PathVariable Integer beitragId, @PathVariable Long userId,@ModelAttribute("sessionUser") User sessionUser) {
        Kommentar kommentar = new Kommentar(kommentarDTO.getKommentartext());
        Beitrag beitrag;
        Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
        if (optionalBeitrag.isPresent()){
            beitrag = optionalBeitrag.get();
            kommentar.setBeitrag(beitrag);
            kommentarRepository.save(kommentar);
        }
        //Optional<User> optionalUser = userRepository.findById(sessionUser.getId());
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()){
            User user = optionalUser.get();
            kommentar.setUser(user);
            kommentarRepository.save(kommentar);
        }
        return "redirect:/beitraguebersicht";
 }
}

