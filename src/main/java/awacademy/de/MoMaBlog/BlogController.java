package awacademy.de.MoMaBlog;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class BlogController {


    @GetMapping("/")
    public String showBeitragForm(Model model) {
        return "home";
    }
    
}
