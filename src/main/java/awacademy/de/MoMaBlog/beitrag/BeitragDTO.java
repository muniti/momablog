package awacademy.de.MoMaBlog.beitrag;

import javax.validation.constraints.NotBlank;
import java.time.Instant;

public class BeitragDTO {

    /**
     * Validierungsvorgaben für Variablen inhalt und ueberschrift
     */

    @NotBlank
    private String inhalt;
    @NotBlank
    private String ueberschrift;


    public BeitragDTO(String inhalt, String ueberschrift) {
        this.inhalt = inhalt;
        this.ueberschrift = ueberschrift;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }
}
