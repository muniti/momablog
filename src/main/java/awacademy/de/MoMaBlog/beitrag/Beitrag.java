package awacademy.de.MoMaBlog.beitrag;

import awacademy.de.MoMaBlog.kommentare.Kommentar;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Beitrag {


    /**
     * @Id: setzt den Primary Key einer Entität
     * @Generated Value: generiert den Wert des Primary Keys automatisch
     */

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String ueberschrift;
    private String inhalt;
    private Instant postedAt;

    @OneToMany(mappedBy = "beitrag")
    private List<Kommentar> kommentarListe;

    public List<Kommentar> getKommentare() {
        return kommentarListe;
    }

    public Beitrag() {
    }

    public Beitrag(String ueberschrift, String inhalt , Instant postedAt){
        this.ueberschrift = ueberschrift;
        this.inhalt = inhalt;
        this.postedAt = postedAt;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }
}
