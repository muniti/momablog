package awacademy.de.MoMaBlog.beitrag;

import awacademy.de.MoMaBlog.beitrag.Beitrag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeitragRepository extends CrudRepository<Beitrag, Integer> {



    /**
     *
     * @return  folgende Methode ermöglicht das Auslesen von Daten aus dem BeitragRepository (sortiert nach dem Beitragspost-Zeitpunkt)
     */
    List<Beitrag> findAllByOrderByPostedAtDesc();
}
