package awacademy.de.MoMaBlog.beitrag;

import awacademy.de.MoMaBlog.kommentare.Kommentar;
import awacademy.de.MoMaBlog.kommentare.KommentarRepository;
import awacademy.de.MoMaBlog.nutzer.User;
import awacademy.de.MoMaBlog.nutzer.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Controller
public class BeitragController {

    private BeitragRepository beitragRepository;
    private UserRepository userRepository;
    private KommentarRepository kommentarRepository;


    /**
     * @param beitragRepository
     * @Autowired erzeugt zur Laufzeit per Depedency Injection eine Verbindung zur Datenbank
     */
    @Autowired
    public BeitragController(BeitragRepository beitragRepository, UserRepository userRepository, KommentarRepository kommentarRepository) {
        this.beitragRepository = beitragRepository;
        this.userRepository = userRepository;
        this.kommentarRepository = kommentarRepository;
    }


    /**
     * im folgenden Getmapping werden Beiträge in chronologischer Reihenfolge (neueste zuerst) angezeigt
     *
     * @param model ist zuständig für die Übermittlung von Daten zwischen Backend und Frontend
     * @return gibt die Html Datei in "BeitragUebersicht" zurück
     */

    @GetMapping("/beitraguebersicht")
    public String showBeitragForm(Model model) {
        BeitragDTO beitragDTO = new BeitragDTO("", "");
        model.addAttribute("beitragDTO", beitragDTO);
        model.addAttribute("beitragsList", beitragRepository.findAllByOrderByPostedAtDesc());
        return "BeitragUebersicht";
    }


    @GetMapping("/beitraghinzufuegen")
    public String showBeitragHinzufuegenForm(Model model) {
        BeitragDTO beitragDTO = new BeitragDTO("", "");
        model.addAttribute("beitragDTO", beitragDTO);
        model.addAttribute("beitragsList", beitragRepository.findAllByOrderByPostedAtDesc());
        return "BeitragHinzufuegen";
    }


    /**
     * folgende Methode ermöglicht das Hinzufügen eines Beitrags
     *
     * @param beitragDTO    ist für kurzlebige Daten von Front- ins Backend
     * @param bindingResult ist für die Überprüfung bzw. Validierung von Eingabedaten notwendig
     * @return gibt nach dem PRG Pattern eine Umleitung
     */
    @PostMapping("/beitragsubmitted")
    public String showBeitragHinzufuegen(@Valid @ModelAttribute BeitragDTO beitragDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "BeitragHinzufuegen";
        }

        Beitrag beitrag = new Beitrag(beitragDTO.getUeberschrift(), beitragDTO.getInhalt(), Instant.now());
        beitragRepository.save(beitrag);
        return "redirect:/beitragsubmitted";
    }

    @GetMapping("/beitragsubmitted")
    public String clubSubmitSuccess() {
        return "BeitragSubmitted";
    }


    /**
     * @param beitragId ist eine PathVariable, d.h. hier kann ich unterschiedliche Werte der BeitragsId einsetzen,
     *                  welche aus der Datenbank entnommen werden und daher voerst auf Optional gesetzt sind und auf Vorhandensein geprüft werden
     * @return
     */

    @GetMapping("/beitrag/{beitragId}")
    public String showBeitrag(Model model, @PathVariable int beitragId) {
        Beitrag beitrag;
        Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
        if (optionalBeitrag.isPresent()) {
            beitrag = optionalBeitrag.get();
            model.addAttribute("beitrag", beitrag);
        }
        return "Beitrag";
    }

    @PostMapping("/beitrag/{beitragId}/{kommentarId}")
    public String kommentarLoeschen(@PathVariable int beitragId, @PathVariable int kommentarId, @ModelAttribute("sessionUser") User sessionUser) {
        if (sessionUser != null) {
            Optional<User> optionalUser = userRepository.findById(sessionUser.getId());

            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                kommentarRepository.deleteById(kommentarId);
                return "redirect:/beitrag/" + beitragId;
            }
        }

        return "redirect:/beitrag/" + beitragId;
    }

    @GetMapping("/beitrag/edit/{beitragId}")
    public String editBeitrag(Model model, @PathVariable int beitragId) {
        Beitrag beitrag;
        Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
        if (optionalBeitrag.isPresent()) {
            beitrag = optionalBeitrag.get();
            model.addAttribute("beitrag", beitrag);
            //model.addAttribute("beitragDTO", new BeitragDTO("", ""));
        }
        return "BeitragEdit";
    }

    @PostMapping("/beitragEdited/{beitragId}")
    public String beitragBearbeiten(@Valid @ModelAttribute BeitragDTO beitragDTO, BindingResult bindingResult, @PathVariable int beitragId) {

        if (bindingResult.hasErrors()) {
            return "BeitragEdit";
        }
        Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
        if (optionalBeitrag.isPresent()) {
            Beitrag beitrag = optionalBeitrag.get();
            beitrag.setInhalt(beitragDTO.getInhalt());
            beitrag.setUeberschrift(beitragDTO.getUeberschrift());
            beitragRepository.save(beitrag);
        }
        return "redirect:/beitragsubmitted";
    }

    @GetMapping("/beitragSuccess")
    public String deletedBeitrag() {
        return "BeitragSuccessDeleted";
    }

//    @PostMapping("/beitraguebersicht")
//    public String beitragLoeschen(@ModelAttribute("sessionUser") User sessionUser, @ModelAttribute("beitragId") int beitragId, @ModelAttribute("kommentarId") int kommentarId) {
//        if (sessionUser != null && sessionUser.isAdmin()) {
//            Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
//            Optional<Kommentar> optionalKommentar = kommentarRepository.findById(beitragId);
//            if (optionalBeitrag.isPresent()) {
//                if (optionalKommentar.isPresent()) {
//                    kommentarRepository.delete(optionalKommentar.get());
//                }
//                beitragRepository.delete(optionalBeitrag.get());
//            }
//        }
//        return "redirect:/beitrag/delete/" + beitragId;
//    }

    @PostMapping("/beitrag/delete/{beitragId}")
    public String beitragLoeschen(@PathVariable int beitragId,@ModelAttribute("sessionUser") User sessionUser) {
        if (sessionUser != null && sessionUser.isAdmin()) {
          Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
          if(optionalBeitrag.isPresent()){
            Beitrag beitrag = optionalBeitrag.get();
            if(sessionUser.isAdmin()){
              List<Kommentar> kommentarListe = beitrag.getKommentare();
              kommentarRepository.deleteAll(kommentarListe);
            }
          }
//            List<Kommentar> kommentarListe = kommentarRepository.findAllByBeitragId(beitragId);
//            for (Kommentar k : kommentarListe) {
//                kommentarRepository.delete(k);
//            }
            beitragRepository.deleteById(beitragId);
//            Optional<Beitrag> optionalBeitrag = beitragRepository.findById(beitragId);
//
//            Optional<Kommentar> kommentarList = kommentarRepository.findById(beitragId);
//            if (optionalBeitrag.isPresent()) {
//                if (optionalKommentar.isPresent()) {
//                    Kommentar kommentar = optionalKommentar.get();
//                    kommentarRepository.deleteById(beitragId);
//                }
//                Beitrag beitrag = optionalBeitrag.get();
//                beitragRepository.deleteById(beitragId);
//            }
        }

        return "redirect:/beitragSuccess";
    }


}
